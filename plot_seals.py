#!/usr/bin/python

import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import glob
import datetime
import ogr
from mpl_toolkits.basemap import pyproj
import matplotlib.animation as animation

def get_date(juld):
    """
    Takes a numpy array or float and returns a list of datetime objects

    Input values should be julian days since 1.1.1950

    Not designed to work with list input
    """
    year = juld//365.25+1950.
    doy = juld%365.25
    if np.size(juld)>1:
        date = [datetime.datetime(int(x),1,1)+datetime.timedelta(int(y) - 1)
                for x,y in np.array([year,doy]).T]
    else:
        date = datetime.datetime(int(year),1,1)+datetime.timedelta(int(doy) - 1)
    return date

def from_to(p,m,x,y):
    """
    Translate map coords x,y in projection p to plot coords in projection m

    p can be defined using eg. pyproj.Proj("+init=EPSG:4326")
    m is a Basemap plot
    """
    lon, lat = p(x,y,inverse=True)
    x_new,y_new = m(lon, lat)
    return x_new, y_new

def check_align(x,y,latlon=False):
    """
    input np.arrays x and y containing coords of points. 
    Returns an array where 0 is a point identified as being in line with
    adjacent points, otherwise 1

    Can take lat lon or map coords - use latlon flag to select

    See comments for method
    """

    # use cross product to determine if point is on a straight line
    # between points on either side ie. o-----x-----o
    # cross product close to zero = on the same line

    plot_align = False

    all_cross = []
    interpd_mask = np.ones(len(x))
    
    # need to cycle through all points
    for i in range(len(x)-1):

        dxc = x[i] - x[i-1]
        dyc = y[i] - y[i-1]

        dxl = x[i+1] - x[i-1]
        dyl = y[i+1] - y[i-1]

        cross = dxc * dyl - dyc * dxl

        all_cross.append(abs(cross))

        # can either test on lat lon or on maps coords
        # however, projections may change straight line to curve
        # interpolation probably done on lat lon, so prob best to use here
        # maybe check this later

        if latlon:
            # thresh for lat lon
            straight_line_thresh = 1.e-10
        else:
            # thresh for map coords
            straight_line_thresh = 500.

        if abs(cross) < straight_line_thresh:

            # mask out value
            interpd_mask[i] = 0

            # plot if required
            if plot_align:
                if latlon:
                    # if in lat lon need transfrom to plot in map coords
                    x1,y1 = m(x[i-1],y[i-1])
                    xc,yc = m(x[i],y[i])
                    x2,y2 = m(x[i+1],y[i+1])
                    m.plot(xc,yc,'o')
                    m.plot([x1, x2], [y1, y2])
                else:
                    # already in map coords, so just plot
                    m.plot(x[i],y[i],'o')
                    m.plot([x[i+1], x[i-1]], [y[i+1], y[i-1]])
    '''
    # some junk left from tring to see if a threshold for cross was easy
    # to identify, not this way...
    fig = plt.figure(2)
    all_cross = np.array(all_cross)

    ax = fig.add_subplot(111)
    binBounds = np.linspace(0,1.e3,10)
    ax.hist(all_cross[~np.isnan(all_cross)],bins=binBounds)
    
    #print all_cross

    #plt.figure(2)
    #plt.hist(all_cross)
    '''

    return interpd_mask

def read_seal_file(fname,m):
    """
    Input filename (fname) and Basemap plot (m)
    Returns numpy arrays containing juld, lat, lon, sal, temp, pres

    Returned data have been filtered to remove data flagged as bad quality,
    data points which lie on a straight line (assumed interpolated) and data
    outside the area of interest.
    """

    PLOT_BAD_QC_FLAG = False

    seal_data = Dataset(fname)

    # Read useful variables
    temp = seal_data.variables['TEMP_ADJUSTED']   
    sal = seal_data.variables['PSAL_ADJUSTED']
    #oxy = seal_data.variables['OXYGEN_ADJUSTED']
    pres = seal_data.variables['PRES_ADJUSTED']
 
    # Get the seal id into a useful string
    seal_id = "".join(seal_data.variables['PLATFORM_NUMBER'][:].compressed())
    print "------------"
    print seal_id

    juld = seal_data.variables['JULD']
    ref_date = seal_data.variables['REFERENCE_DATE_TIME']

    cyc_no = seal_data.variables['CYCLE_NUMBER']

    lat = seal_data.variables['LATITUDE'][:]
    lon = seal_data.variables['LONGITUDE'][:]

    # get data shape (for temp, sal, pres)
    [n_levels, n_profs] = temp.shape
    print "Number of dives recorded:", n_profs

    # convert date
    date_array = get_date(juld[:])

    # load all QC data to make overall qc mask
    # this may be overkill
    qc_mask = np.ones((n_levels, n_profs))

    # Read quality control data
    all_qcs = ['TEMP_ADJUSTED_QC',
               'PSAL_ADJUSTED_QC',
               #'OXYGEN_ADJUSTED_QC',
               'PRES_ADJUSTED_QC']

    for qc_var in all_qcs:
        this_qc = seal_data.variables[qc_var]
        this_qc = np.array(this_qc).astype(np.float)
        # Discard QC value greater than two
        # see: http://www.argodatamgt.org/content/download/15699/102401/file/argo-quality-control-manual-version2.8.pdf
        qc_mask[this_qc>2]=0
        
    # if any values in cast are flagged as bad in QC get rid of whole cast
    # may be a little overkill
    prof_qc_mask = np.min(qc_mask,axis=0) 
    print ("Removed %s points flagged as bad data"% \
               len(prof_qc_mask[prof_qc_mask==0]))

    #convert lat lon to map coords
    x,y = m(lon,lat)

    # get a mask to remove interpolated data
    interpd_mask = check_align(lon,lat,latlon=True)
    print "Removed %s interpolated points "%len(interpd_mask[interpd_mask==0])

    # area mask
    area_mask = np.ones(n_profs)
    
    # define area mask limits
    lat_max = 78.95
    lat_min = 78.85
    lon_max = 12.25
    lon_min = 12.75

    x_kmin,y_kmin = m(lon_max,lat_max)
    x_kmax,y_kmax = m(lon_min,lat_min)

    m.plot([x_kmin,x_kmax,x_kmax,x_kmin,x_kmin],
           [y_kmin,y_kmin,y_kmax,y_kmax,y_kmin])

    area_mask[lon>lon_min] = 0
    area_mask[lon<lon_max] = 0
    area_mask[lat>lat_max] = 0
    area_mask[lat<lat_min] = 0

    print ("Removed %s points not from Kronebreen"% \
               len(area_mask[area_mask==0]))
    
    # combine both masks
    total_mask = np.min([interpd_mask,prof_qc_mask,area_mask],axis=0)
    print "Leaving %s good dives" % len(total_mask[total_mask==1])

    # optional plotting of bad QC data
    if PLOT_BAD_QC_FLAG:
        m.scatter(x[total_mask==0],y[total_mask==0],color='r',marker='x')

    return [juld[total_mask==1], lat[total_mask==1], lon[total_mask==1],\
            sal[:,total_mask==1], temp[:,total_mask==1], pres[:,total_mask==1]]

def setup_figure():
    """
    Set up a basemap plot and draw ice fronts

    """

    fig = plt.figure()
    ax1 = plt.subplot2grid((2,3), (0, 0), rowspan=2, colspan=2)
    ax2 = plt.subplot2grid((2,3), (0, 2))
    ax3 = plt.subplot2grid((2,3), (1, 2))

    plt.sca(ax1)
    # set up projection + area
    m = Basemap(projection='merc',
                lon_0=15,
                lat_0=90,
                #lat_ts=(max(lat)+min(lat))/2.,
                #llcrnrlat=78.8,urcrnrlat=79.1,
                #llcrnrlon=12,urcrnrlon=14,
                # larger area
                #llcrnrlat=78.5,urcrnrlat=79.4,
                #llcrnrlon=11,urcrnrlon=14,
                # Kronebreen box
                llcrnrlat=78.85,urcrnrlat=78.95,
                llcrnrlon=12.25,urcrnrlon=12.75,
                resolution='f')

    # draw generic coastline
    #m.drawcoastlines()

    # or draw terminus outlines from Jack
    # set shapefile projection
    p = pyproj.Proj("+init=EPSG:4326")

    fname = '/home/alistair/Field_data/Kronebreen/Fronts/FP_KRB_KNG_XL_ALL.shp'
    shapef = ogr.Open(fname)
    lyr = shapef.GetLayer()
    for feature in range(lyr.GetFeatureCount()-1):
        poly = lyr.GetNextFeature()
        geom = poly.GetGeometryRef()
        pts = geom
        points = np.array(pts.GetPoints())
        try:
            new_points = np.array(m(points[:,0],points[:,1]))
            m.plot(new_points[0],new_points[1],alpha=0.5)
        except:
            continue

    # add lat lon labels
    m.drawparallels(np.arange(78,80,.02), labels=[1,0,0,0])
    m.drawmeridians(np.arange(10,15.,0.1), labels=[0,0,1,1])

    ax2.set_xlim(-2,10)
    ax2.set_ylim(0,100)
    ax2.invert_yaxis()

    ax3.set_xlim(15,36)
    ax3.set_ylim(0,100)
    ax3.invert_yaxis()

    plt.tight_layout()

    return fig,ax1,ax2,ax3,m

def compile_seal_data(folder):

    #get seals list
    seals = glob.glob(folder)

    # initialise some lists/arrays
    all_juld = []
    all_lat = []
    all_lon = []
    all_sal = np.empty((18,0))
    all_temp = np.empty((18,0))
    all_pres = np.empty((18,0))

    # cycle through seal files
    for s in seals:

        # filter to ignore data not from 2012
        if s[-9:-7] != '12': continue

        # get seal data from this file
        out = read_seal_file(s,m)

        # some of the casts have 17 rather than 18 levels
        # so add an extra no data level to these so the arrays are the same size
        if len(out[3]) < 18:
            out[3:]=[np.append(x,np.ones((1,len(x[0,:])))*np.nan,axis=0) 
                     for x in out[3:]]

        # add to combined data
        all_juld += list(out[0])
        all_lat += list(out[1])
        all_lon += list(out[2])

        all_sal = np.append(all_sal,np.array(out[3][:18]),axis=1)
        all_temp = np.append(all_temp,np.array(out[4][:18]),axis=1)
        all_pres = np.append(all_pres,np.array(out[5][:18]),axis=1)

    # change no data value to nan
    all_sal[all_sal==99999] = np.nan
    all_temp[all_temp==99999] = np.nan
    all_pres[all_pres==99999] = np.nan

    # how many dives?
    n_dives = len(all_juld)

    print "-----------"
    print "Got %s good dives :)" % n_dives

    # change to numpy arrays
    all_juld = np.array(all_juld)
    all_lat = np.array(all_lat)
    all_lon = np.array(all_lon)
    all_sal = np.array(all_sal)
    all_temp = np.array(all_temp)
    all_pres = np.array(all_pres)

    # reorder by date
    date_order = all_juld.argsort()

    all_juld = all_juld[date_order]
    all_lat = all_lat[date_order]
    all_lon = all_lon[date_order]
    all_sal = all_sal[:,date_order]
    all_temp = all_temp[:,date_order]
    all_pres = all_pres[:,date_order]

    # lat lon to x y
    all_x,all_y = m(all_lon,all_lat)

    return [all_juld,all_x,all_y,all_sal,all_temp,all_pres]

def init_animation(seal_data):
    
    # unpack seal data
    [all_juld,all_x,all_y,all_sal,all_temp,all_pres] = seal_data

    # define a color based on the days
    cols = plt.cm.jet((all_juld-22872.)/(23129-22872))

    pts = []
    t_plot = []
    s_plot = []

    # generate empty lines to add data to later
    for j in range(len(all_juld)):
        pobj = ax1.plot([],[],color=cols[j],marker='o',zorder=2)[0]
        pts.append(pobj)

        tobj = ax2.plot([],[],alpha=1,color=cols[j], lw=2, zorder=2)[0]
        t_plot.append(tobj)

        sobj = ax3.plot([],[],alpha=1,color=cols[j], lw=2, zorder=2)[0]
        s_plot.append(sobj)

    p=0

    # set data for first line
    pts[p].set_data(all_x[p],all_y[p])
    t_plot[p].set_data(all_temp[:,p], all_pres[:,p])
    s_plot[p].set_data(all_sal[:,p], all_pres[:,p])

    return p,pts,t_plot,s_plot

def updatefig(*args):
    """
    Function for animating plot used with animation.FuncAnimation()
    """
    global p
    p+=1

    # for each point draw current data and set previous data to grey
    pts[p].set_data(all_x[p],all_y[p])
    pts[p-1].set_color('0.6')
    pts[p-1].set_markersize(1)

    t_plot[p].set_data(all_temp[:,p], all_pres[:,p])
    t_plot[p-1].set_color('0.1')
    t_plot[p-1].set_lw(0.1)
    t_plot[p-1].set_alpha(0.5)

    s_plot[p].set_data(all_sal[:,p], all_pres[:,p])
    s_plot[p-1].set_color('0.1')
    s_plot[p-1].set_lw(0.1)
    s_plot[p-1].set_alpha(0.5)


    return tuple(t_plot) + tuple(s_plot) + tuple(pts)

if __name__ == '__main__':

    fig,ax1,ax2,ax3,m = setup_figure()

    # folder containing seal data
    seal_folder = "/home/alistair/Field_data/Kronebreen/seal_data/*CTD.nc"

    # gather all seal data together
    seal_data = compile_seal_data(seal_folder)

    # initialise animation
    p,pts,t_plot,s_plot = init_animation(seal_data)

    [all_juld,all_x,all_y,all_sal,all_temp,all_pres] = seal_data

    # animate
    ani = animation.FuncAnimation(fig, updatefig, interval=1, 
                                  #fargs = args,
                                  frames=len(all_juld)-1, 
                                  repeat=False, blit=True)

    plt.show()

